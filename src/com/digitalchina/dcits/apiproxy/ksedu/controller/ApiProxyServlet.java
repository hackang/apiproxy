
package com.digitalchina.dcits.apiproxy.ksedu.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

/**
 * @author yuanyukang
 *
 */
@WebServlet(name = "apiProxyServlet", urlPatterns = "/servlet/edu/apiProxy")
public class ApiProxyServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8815171736502248403L;

	protected void doGet(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
			throws ServletException, IOException {

		try {
			this.getProxy(httpRequest, httpResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
			throws ServletException, IOException {
		Enumeration<String> names = httpRequest.getParameterNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			if ("proxyUrl".equals(name)) {
				continue;
			}
			System.out.println("request name : " + name);
			String value = httpRequest.getParameter(name);
			System.out.println("request value : " + value);
		}
		try {
			this.postProxy(httpRequest, httpResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// httpResponse.getWriter().write("{\"status\":\"n\",\"message\":\"用户名密码不匹配\"}");
	}

	private void postProxy(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws Exception {

		String proxyUrl = httpServletRequest.getParameter("proxyUrl");
		System.out.println("new post request, proxyUrl : " + proxyUrl);
		if (StringUtils.isBlank(proxyUrl)) {
			httpServletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, "Post Url can not be empty");
			return;
		}
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			HttpPost httpPost = new HttpPost(proxyUrl);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			Enumeration<String> names = httpServletRequest.getParameterNames();
			while (names.hasMoreElements()) {
				String name = names.nextElement();
				if ("proxyUrl".equals(name)) {
					continue;
				}
				String value = httpServletRequest.getParameter(name);
				nvps.add(new BasicNameValuePair(name, value));
			}
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			CloseableHttpResponse response = httpclient.execute(httpPost);
			try {
				// 获取响应实体
				HttpEntity entity = response.getEntity();
				System.out.println("--------------------------------------");
				// 打印响应状态
				System.out.println(response.getStatusLine());
				if (entity != null) {
					// 打印响应内容长度
					System.out.println("Response content length: " + entity.getContentLength());
					// 打印响应内容
					String content = EntityUtils.toString(entity);
					System.out.println("Response content: " + content);
					httpServletResponse.getWriter().write(content);
				}
				System.out.println("------------------------------------");
			} finally {
				response.close();
			}
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void getProxy(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws Exception {

		String proxyUrl = httpServletRequest.getParameter("proxyUrl");
		System.out.println("new get request, proxyUrl : " + proxyUrl);
		if (StringUtils.isBlank(proxyUrl)) {
			// getUrl =
			// "http://edu.wiseks.net:1211/index.php?m=edu&c=api&catid=2&limit=10&a=contentlist";
			httpServletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, "Get Url can not be empty");
			return;
		}
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			HttpGet httpGet = new HttpGet(proxyUrl);
			CloseableHttpResponse response = httpclient.execute(httpGet);
			try {
				// 获取响应实体
				HttpEntity entity = response.getEntity();
				System.out.println("--------------------------------------");
				// 打印响应状态
				System.out.println(response.getStatusLine());
				if (entity != null) {
					// 打印响应内容长度
					System.out.println("Response content length: " + entity.getContentLength());
					// 打印响应内容
					String content = EntityUtils.toString(entity);
					System.out.println("Response content: " + content);
					httpServletResponse.getWriter().write(content);
				}
				System.out.println("------------------------------------");
			} finally {
				response.close();
			}
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
